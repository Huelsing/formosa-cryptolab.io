---
name: Miguel Quaresma
institution: Max Planck Institute for Security and Privacy
website: https://mquaresma.github.io/about/

projects: EasyCrypt, Jasmin, Libjade
---