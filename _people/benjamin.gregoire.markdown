---
name: Benjamin Grégoire
institution: Inria Sophia-Antipolis Méditerranée
website: http://www-sop.inria.fr/members/Benjamin.Gregoire/

projects: EasyCrypt, Jasmin
---
