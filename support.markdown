---
layout: default

title: Formosa Supporters
permalink: support/
---

# Funded Projects

{% for grant in site.funding %}
- **{{ grant.name }}** ({{ grant.date | date: '%b %Y'}}—{{ grant.end | date: '%b %Y'}}): {{ grant.funder }}, {{ grant.programme }}, {{ grant.number }}<br/>
  {{ grant.content | markdownify }}
{% endfor %}
