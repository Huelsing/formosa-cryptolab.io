---
layout: default
title: Typing High-Speed Cryptography against Spectre v1
year: 2022
authors: Basavesh Ammanaghatta Shivakumar, Gilles Barthe, Benjamin Grégoire, Vincent Laporte, Tiago Oliveira, Swarn Priya, Peter Schwabe, Lucas Tabary-Maujean
website: https://eprint.iacr.org/2022/1270
conference: IACR, 2022

---