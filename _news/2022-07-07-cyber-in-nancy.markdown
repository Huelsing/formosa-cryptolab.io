---
layout: default

title: Jasmin lecture in Nancy
date: 2022-07-07

summary: >-
  During the [Cyber in Nancy](https://cyber-in-nancy.loria.fr/) summer school,
  Vincent Laporte and Benjamin Grégoire gave a lecture on Jasmin.

linkbak: >-
  Read the full announcement.
---

# Jasmin lecture at Cyber in Nancy

As part of the [Cyber in Nancy](https://cyber-in-nancy.loria.fr/) summer school,
a few dozens Ph.D. students learned how to write constant-time programs in Jasmin.

The lecture has been followed by a hands-on exercise session.

The [lecture material](/assets/files/2022/07/nancy/CyberIn2022_JasminExercises.tar.bz2) contains:

  - Jasmin example programs, discussed during the lecture;
  - additional documentation on the Jasmin language and tools;
  - exercises for the practical session (with corrections).
