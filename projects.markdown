---
layout: page
title: Projects

permalink: /projects/
---

{% for project in site.projects %}
- [**{{ project.project }}**]({{ project.url | relative_url }}) — [Project Website]({{ project.wbesite }}) — [Git Repository]({{ project.git }})<br/>
  {{ project.short | markdownify }}
{% endfor %}
